
- Install Anaconda: `https://www.anaconda.com`
- Create root environment: `https://root.cern/install/#conda`
- `conda activate root_environment`
- Install python packages:

```
pip3 install numpy pandas matplotlib
```
